<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Comment;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class CommentFixtures extends Fixture
{

    public const COMMENT_ONE = 'commentOne';
    public const COMMENT_TWO = 'commentTwo';
    public const COMMENT_THREE = 'commentThree';

    public function load(PersistenceObjectManager $manager)
    {
        // Comment One
        $commentOne = new Comment();
        $commentOne->setUsername("Test 1");
        $commentOne->setContent("Super bouquin !");
        $commentOne->setDate(new \Datetime("now"));

        // Comment two
        $commentTwo = new Comment();
        $commentTwo->setUsername("Test 2");
        $commentTwo->setContent("Un bon retour dans le passé !");
        $commentTwo->setDate(new \Datetime("now"));

        // Comment three
        $commentThree = new Comment();
        $commentThree->setUsername("Test 3");
        $commentThree->setContent("Youhouuu !");
        $commentThree->setDate(new \Datetime("now"));

        $manager->persist($commentOne);
        $manager->persist($commentTwo);
        $manager->persist($commentThree);

        $manager->flush();

        $this->addReference(self::COMMENT_ONE, $commentOne);
        $this->addReference(self::COMMENT_TWO, $commentTwo);
        $this->addReference(self::COMMENT_THREE, $commentThree);

    }
}
