<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Post;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerInterface;
use App\Repository\PostRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

class PostController extends AbstractController
{
    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    // /**
    //  * @Route("/posts", methods="GET")
    //  */
    // public function allPosts(PostRepository $postRepo)
    // {
    //     $posts = $postRepo->findAll();
    //     $json = $this->serializer->serialize($posts, 'json');

    //     return new JsonResponse($json, 200, [], true);
    // }

    /**
     * @Route("/posts", methods="GET")
     */
    public function allPostsByDesc(PostRepository $postRepo, Request $request)
    {
        $posts = $postRepo->findByDesc($request->get('title'));
        $json = $this->serializer->serialize($posts, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("post/{post}", methods="GET")
     */
    public function onePost(Post $post)
    {
        return new JsonResponse($this->serializer->serialize($post, 'json'), 200, [], true);
    }
}
