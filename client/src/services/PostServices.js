import Axios from 'axios';

export default class PostService {
  constructor() {
    this.url = 'http://localhost:8000/';
  }

  async findAll() {
    const posts = await Axios.get(this.url + 'posts/');
    return posts.data;
  }
  
  async findById(routeParam) {
    const post = await Axios.get(this.url + 'post/' + routeParam);
    post.data.comments.sort().reverse(); 
    return post.data;
  }
}
